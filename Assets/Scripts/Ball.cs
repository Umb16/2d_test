﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour
{
    static int level = 0;

    void Start()
    {
        
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Obstacle")
        {
            SceneManager.LoadScene(Application.loadedLevel);
        }
        if (other.gameObject.tag == "Gates")
        {
            GameAnalytics.NewDesignEvent($"Level_{level}_complete");
            level++;
            if (SceneManager.sceneCountInBuildSettings == level)
                level = 0;

            SceneManager.LoadScene(level);
        }
    }
}
