﻿using GameAnalyticsSDK;
using GameAnalyticsSDK.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DragArea : MonoBehaviour
{
    [SerializeField]
    Rigidbody ball;
    [SerializeField]
    Transform arrow;
    bool startDrag;
    Vector2 startDragPos;


    // Update is called once per frame
    void Update()
    {
        transform.position = ball.transform.position;
        if (startDrag)
        {
            Vector2 direction = ((Vector2)Input.mousePosition - startDragPos) / Screen.height * 1000;
            arrow.localScale = new Vector3(1, direction.magnitude / 50, 1);
            arrow.eulerAngles = new Vector3(0, 0, Vector2.SignedAngle(Vector3.up, direction));
            if (Input.GetMouseButtonUp(0))
            {
                arrow.localScale = new Vector3(1, .5f, 1);
                startDrag = false;
                ball.AddForce(direction);
            }
        }
    }

    private void OnMouseDown()
    {
        startDrag = true;
        startDragPos = Input.mousePosition;
    }

   
}
